﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class AdministrativeStaff : Person
    {
        public static List<AdministrativeStaff> List { get; set; }

        public static string ShowAll()
        {
            string text = "Lijst van Administratief personeel:\n";
            foreach (var adminstratie in AdministrativeStaff.List)
            {
                text += $"{adminstratie.FirstName} {adminstratie.LastName} {adminstratie.Birthday:yyyy-MM-dd} {adminstratie.Id} {adminstratie.SchoolId}";
            }
            return text;
        }
        public override string ShowOne()
        {
            return $"Gegevens van een Administratief personeel: {this.FirstName}, {this.LastName}, {this.Id}, {this.SchoolId}";
        }

        public override string GetNameTagText()
        {
            return $"(ADMINISTRATIE): {this.FirstName} {this.LastName}";
        }
    }
}
