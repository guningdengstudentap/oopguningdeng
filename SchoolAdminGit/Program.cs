﻿using System;
using System.Collections.Generic;

namespace SchoolAdmin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("schoolbeheer");
            School school1 = new School();
            school1.Name = "GO! BS de Spits";
            school1.Street = "Thonetlaan 106";
            school1.PostalCode = "2050";
            school1.City = "Antwerpen";
            school1.Id = 1;

            School school2 = new School();
            school2.Name = "GO! Koninklijk Atheneum Deurne";
            school2.Street = "Fr. Craeybeckxlaan 22";
            school2.PostalCode = "2100";
            school2.City = "Deurne";
            school2.Id = 2;

            List<School> schoolList = new List<School>();
            schoolList.Add(school1);
            schoolList.Add(school2);

            School.List = schoolList;
            Console.WriteLine(School.ShowAll());
            Console.ReadLine();

            #region Student
            Student student1 = new Student();
            student1.FirstName = "Mohamed";
            student1.LastName = "El Farisi";
            student1.Birthday = new DateTime(1987, 12, 06);
            student1.Id = 1;
            student1.SchoolId = 1;

            Student student2 = new Student();
            student2.FirstName = "Sharah";
            student2.LastName = "Jansens";
            student2.Birthday = new DateTime(1991, 10, 21);
            student2.Id = 2;
            student2.SchoolId = 1;

            Student student3 = new Student();
            student3.FirstName = "Bart";
            student3.LastName = "Jansens";
            student3.Birthday = new DateTime(1990, 10, 21);
            student3.Id = 2;
            student3.SchoolId = 3;

            Student student4 = new Student();
            student4.FirstName = "Farah";
            student4.LastName = "El Farisi";
            student4.Birthday = new DateTime(1987, 12, 06);
            student4.Id = 1;
            student4.SchoolId = 4;

            List<Student> StudentsList = new List<Student>();
            StudentsList.Add(student1);
            StudentsList.Add(student2);
            StudentsList.Add(student3);
            StudentsList.Add(student4);

            Student.List = StudentsList;
            Console.WriteLine(Student.ShowAll());
            Console.ReadLine();
            Console.WriteLine(student1.ShowOne());
            Console.ReadLine();
            #endregion

            #region lecturer
            var lecturer1 = new Lecturer();
            lecturer1.FirstName = "Adem";
            lecturer1.LastName = "Kaye";
            lecturer1.Birthday = new DateTime(1976, 12, 01);
            lecturer1.Id = 1;
            lecturer1.SchoolId = 1;
            var lecturer2 = new Lecturer();
            lecturer2.FirstName = "Anne";
            lecturer2.LastName = "Wouters";
            lecturer2.Birthday = new DateTime(1968, 04, 03);
            lecturer2.Id = 2;
            lecturer2.SchoolId = 2;

            Lecturer.List = new List<Lecturer>();
            Lecturer.List.Add(lecturer1);
            Lecturer.List.Add(lecturer2);
            Console.WriteLine(Lecturer.ShowAll());
            Console.WriteLine(lecturer1.ShowOne());
            Console.ReadLine();
            #endregion

            #region AdministrativeStaff
            var administratie = new AdministrativeStaff();
            administratie.FirstName = "Raul";
            administratie.LastName = "Jacob";
            administratie.Birthday = new DateTime(1985, 11, 01);
            administratie.Id = 1;
            administratie.SchoolId = 1;

            AdministrativeStaff.List = new List<AdministrativeStaff>();
            AdministrativeStaff.List.Add(administratie);
            Console.WriteLine(AdministrativeStaff.ShowAll());
            Console.WriteLine(administratie.ShowOne());
            Console.ReadLine();
            #endregion

            Console.WriteLine(student1.GetNameTagText()); //
            Console.WriteLine(lecturer1.GetNameTagText()); //
            Console.WriteLine(administratie.GetNameTagText()); //
            Console.WriteLine();

            #region print alle Person
            Console.WriteLine("Toon alle personeel samen:");
            var persons = new List<Person>();
            persons.Add(student1);
            persons.Add(lecturer1);
            persons.Add(administratie);
            foreach (var person in persons)
            {
                Console.WriteLine(person.GetNameTagText());
            }

            Console.ReadLine();
            #endregion

        }
    }
}
