﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Lecturer : Person
    {
		public static List<Lecturer> List { get; set; }

		public static string ShowAll()
		{
			string text = "Lijst van Lecturer:\n";
			foreach (var lecturer in Lecturer.List)
			{
				text += $"{lecturer.FirstName}, {lecturer.LastName}, {lecturer.Birthday:yyyy-MM-dd}, {lecturer.Id}, {lecturer.SchoolId}\r\n";
			}
			return text;
		}

		public override string ShowOne()
		{
			string text = $"Gegevens van een lecturer: {this.FirstName}, {this.LastName}, {this.Birthday:yyyy-MM-dd}, {this.Id}, {this.SchoolId}";
			return text;
		}

		public string ShowTaughtCourses()
		{
			return $"Vakken van deze lector: ???";
		}

		public override string GetNameTagText()
		{
			return $"(LECTOR): {this.FirstName} {this.LastName}";
		}
	}
}
