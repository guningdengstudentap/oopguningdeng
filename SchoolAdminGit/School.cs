﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class School
    {
		private string name;

		public string Name
		{
			get { return name; }
			set
			{
				if (string.IsNullOrEmpty(name))
				{
					name = value;
				}
			}
		}

		public string Street { get; set; }

		public string PostalCode { get; set; }

		public string City { get; set; }

		public int Id { get; set; }

		public static List<School> List { get; set; }

		public static string ShowAll()
		{
			string text = "Lijst van Scholen:\n";
			foreach (var school in List)
			{
				text += $"{school.Name}, {school.Street}, {school.City}, {school.Id}\r\n";
			}
			return text;
		}


		public string showOne()
		{
			string text = $"Gegevens van de school: {this.Name}, {this.Street}, {this.PostalCode}, {this.City}";
			return text;
		}
	}
}
