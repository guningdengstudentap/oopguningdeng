﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolAdmin
{
    class Student : Person
    {
		public static List<Student> List { get; set; }

		public static string ShowAll()
		{
			string text = "Lijst van Students:\n";
			foreach (var student in List)
			{
				text += $"{student.FirstName}, {student.LastName}, {student.Birthday:yyyy-MM-dd}, {student.Id}, {student.SchoolId}\r\n";
			}
			return text;
		}

		public override string ShowOne()
		{
			string text = $"Gegevens van een student: {this.FirstName}, {this.LastName}, {this.Birthday:yyyy-MM-dd}, {this.Id}, {this.SchoolId}";
			return text;
		}
	}
}
