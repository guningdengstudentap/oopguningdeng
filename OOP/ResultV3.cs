﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: properties en methodes.
     * key point 2: if lue binnen read-only property
     *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H8-RapportModule-V3".
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.

    */
    class ResultV3
    {
        public byte Percentage { get; set; }

        public string Honors
        {
            get
            {
                if (Percentage < 50)
                {
                    return $"NietGeslaagd";
                }
                else if (Percentage <= 68)
                {
                    return $"Voldoende";
                }
                else if (Percentage <= 75)
                {
                    return $"Onderscheiding";
                }
                else if (Percentage <= 85)
                {
                    return $"GroteOnderscheiding";
                }
                else
                {
                    return $"GrootsteOnderscheiding";
                }
            }
        }
    }

}
