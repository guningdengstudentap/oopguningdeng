﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Overerving
    {
        public static void DemonstrateWorkingStudent()
        {
            List<Student> studentsList = new List<Student>();
            Student student = new Student();
            WorkingStudent workingStudent = new WorkingStudent();
            Random randomHours = new Random();
            byte workHors = Convert.ToByte(randomHours.Next(26));

            studentsList.Add(new Student()
            {
                Name = "Tom",
                Age = 22,
                ClassGroupPro = Student.ClassGroup.EA1,
                MarkCommunication = 17,
                MarkProgrammingPrinciples = 15,
                MarkWebTech = 16
            });
            studentsList.Add(new WorkingStudent()
            {
                Name = "Popier",
                Age = 26,
                ClassGroupPro = Student.ClassGroup.EA2,
                MarkCommunication = 15,
                MarkProgrammingPrinciples = 15,
                MarkWebTech = 18,
                WorkHours = workHors
            });

            Console.WriteLine("Oefening 1 H12 Een bestaande klasse uitbreiden via overerving\r\n");

            foreach (Student studentX in studentsList)
            {
                Console.WriteLine($"Student naam: {studentX.Name}");
            }
            Console.WriteLine("\r\n+++++++++++++++++++++++++++\r\n");

            Console.WriteLine("Oefening 4: H12 Dynamic dispatch\r\n");
            foreach (Student studentX in studentsList)
            {
                studentX.ShowOverview();
            }
        }

        public static void DemonstrateTasks()
        {
            ArrayList taskArrayList = new ArrayList();
            int choice;
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1, een taak maken.");
            Console.WriteLine("2, een terugkerende taak maken.");
            Console.WriteLine("3, stoppen.");
            Console.WriteLine("\r\n**************************\r\n");
            Console.Write("\r\n> ");
            choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Console.WriteLine("Beschrijving van de taak?");
                    string newTask = Console.ReadLine();
                    Task task1 = new Task(newTask);
                    taskArrayList.Add(task1);
                    foreach(Task x in taskArrayList)
                    {
                        Console.WriteLine($"Taak {task1.Description} is aangemaakt.");
                    }
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 2:
                    Console.WriteLine("Beschrijving van de taak?");
                    Console.Write("\r\n> ");
                    String newRecurringTask = Console.ReadLine();
                    Console.WriteLine("Aantal dagen tussen herhaling?");
                    byte days = Convert.ToByte(Console.ReadLine());
                    RecurringTask recurringTask = new RecurringTask(newRecurringTask, days, "");
                    taskArrayList.Add(recurringTask);
                    foreach(RecurringTask y in taskArrayList)
                    {
                        Console.WriteLine($"Taak {recurringTask.Beschrijving} is aangemaakt.");
                        Console.WriteLine($"Deze taak moet om de {recurringTask.Days} dagen herhaald worden.");
                    }
                    break;
                case 3:
                    Console.ReadKey();
                    Console.Clear();
                    break;                    

            }

        }

        public static void DemostratePatients()
        {
            Console.WriteLine("H12 Ziekenhuis\n\r");
            int choiceNum;
            int stayHours;
            string patientName;
            Console.WriteLine("Geef de naam van patiënt in:");
            patientName = Console.ReadLine();
            Console.WriteLine("Hoeveel uur heeft da patiënt in het ziekenhuis gelegen: ");
            stayHours = Convert.ToInt32(Console.ReadLine());
            while (true)
            {
                Console.ResetColor();
                Console.WriteLine("\n\rWat is de patiët?\n\r");
                Console.WriteLine("1, Gewone patiënt");
                Console.WriteLine("2, Verzekede patiënt");
                Console.WriteLine("3, Exit");
                Console.Write("\n\r***********************************\n\r");
                choiceNum = Convert.ToInt32(Console.ReadLine());
                Console.Write("\n\n\r\r");
                switch (choiceNum)
                {
                    case 1:
                        Patient patient = new Patient();
                        patient.PatientNames = patientName;
                        patient.Hour = stayHours;
                        patient.ShowCost();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        InsuredPatient insuredPatient = new InsuredPatient(patientName, stayHours);
                        insuredPatient.PatientNames = patientName;
                        insuredPatient.Hour = stayHours;
                        insuredPatient.ShowCost();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.WriteLine("Exit");
                        Console.ReadKey();
                        Console.Clear();
                        return;

                    default:
                        Console.WriteLine("Ongeldige keuze!");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }
            }
        }

        public static void StartSubmenuOvereving()
        {
            Console.ResetColor();
            Console.WriteLine("\nWelke oefening wil je uitvoeren?");
            Console.WriteLine("1. Een bestaande klasseuitbreiden via overering");
            Console.WriteLine("2. Klassen met aangepaste constructor maken");
            Console.WriteLine("3. Ziekenhuis");
            Console.WriteLine("4. Dynamic dispatch");

            Console.Write("**************************\n\n\r\r");
            int choice = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n\r");
            switch (choice)
            {
                case 1:
                    DemonstrateWorkingStudent();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 2:
                    DemonstrateTasks();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 3:
                    DemostratePatients();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 4:
                    DemonstrateWorkingStudent();
                    Console.ReadKey();
                    Console.Clear();
                    break;

                default:
                    Console.WriteLine("Ongeldige keuze!");
                    Console.ReadKey();
                    Console.Clear();
                    break;
            }

        }

    }
}
