﻿using System;
using OOP.Geometry;

namespace OOP
{
    /* 
	 * Summay
     *************************************
	 * Guning Deng(student)
	 * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.
     * 2020
             
    */
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("\r\nObject Oriened Pprogramming\r\n");
            int choice;
            while (true)
            {
                Console.ResetColor();
                Console.WriteLine("Wat wil je doen?");
                Console.WriteLine("1. Smaakmaker OOP");
                Console.WriteLine("2. Oefening H8 Klassen en objecten");
                Console.WriteLine("3. Oefening H11 Array en classen");
                Console.WriteLine("4. Oefening H12 Overerving");

                Console.Write("\r\r\n\nKeuzen: ");
                choice = Convert.ToInt32(Console.ReadLine());
                Console.Write("\r\r\n\n");
                switch (choice)
                {
                    case 1:
                        KlassenEnObjecten.StartMenuSmaakmaker();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        KlassenEnObjecten.StartMenuKlassenEnObjecten();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        ArrayEnKlassen.StartSubmenuArrayEnKlssen();
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 4:
                        Overerving.StartSubmenuOvereving();
                        Console.ReadKey();
                        Console.Clear();
                        break;

                    default:
                        Console.WriteLine("Ongeldige Keuze!");
                        Console.ReadKey();
                        Console.Clear();
                        break;
            

                }
            }
                     
        }

        
    }
}
