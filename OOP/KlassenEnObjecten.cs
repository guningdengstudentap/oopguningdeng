﻿using OOP.Geometry;
using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class KlassenEnObjecten
    {

        public static void StartMenuSmaakmaker()
        {
            Console.WriteLine("Smaakmaker OOP\r\n");
            int choice;
            Console.ResetColor();
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. Vormen tekenen (h8-vormen)");
            Console.WriteLine("2. Auto's laten rijden (h8-autos)");
            Console.WriteLine("3. Patienten tonen (h8-patienten)");
            Console.WriteLine("4. Voorbeeld abstractie (h8-honden)");
            Console.Write("\r\r\n\nKeuzen: ");
            choice = Convert.ToInt32(Console.ReadLine());
            Console.Write("\r\r\n\n");
            switch (choice)
            {
                case 1:
                    ShapesBuilder.ShowShapes();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 2:
                    Car.DrivingCars();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 3:
                    Patient.Showpatient();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 4:
                    BarkingDog.Barking();
                    Console.ReadKey();
                    Console.Clear();
                    break;

                default:
                    Console.WriteLine("Ongeldige Keuze!");
                    Console.ReadKey();
                    Console.Clear();
                    break;

            }

        }
        public static void StartMenuKlassenEnObjecten()
        {
            Console.WriteLine("H8 Oefening\r\n");
            int choice;
            Console.ResetColor();
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. H8-dag-van-de-week");
            Console.WriteLine("2. H8-ticks-sinds-2000");
            Console.WriteLine("3. H8-schrikkelteller");
            Console.WriteLine("4. H8-simpele-timing");
            Console.WriteLine("5. H8-RapportModule-V1");
            Console.WriteLine("6. H8-RapportModule-V2");
            Console.WriteLine("7. H8-Getallencombinatie");
            Console.WriteLine("8. H8-Figuren");
            Console.WriteLine("9. H8-StudentKlasse");
            Console.WriteLine("10. H8-RapportModule-V3");

            Console.Write("\r\r\n\nKeuzen: ");
            choice = Convert.ToInt32(Console.ReadLine());
            Console.Write("\r\r\n\n");
            switch (choice)
            {
                case 1:
                    DayOfWeekProgram.DayOfWeek();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 2:
                    DayOfWeekProgram.Ticks2000();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 3:
                    DayOfWeekProgram.LeapYear();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 4:
                    DayOfWeekProgram.ArrayTimer();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 5:
                    ResultV1 resultV1 = new ResultV1();
                    resultV1.Percentage = 48;
                    resultV1.PrintHonos();
                    ResultV1 resultV1a = new ResultV1();
                    resultV1a.Percentage = 55;
                    resultV1a.PrintHonos();
                    ResultV1 resultV1b = new ResultV1();
                    resultV1b.Percentage = 74;
                    resultV1b.PrintHonos();
                    ResultV1 resultV1c = new ResultV1();
                    resultV1c.Percentage = 92;
                    resultV1c.PrintHonos();
                    ResultV1 resultV1d = new ResultV1();
                    resultV1d.Percentage = 81;
                    resultV1d.PrintHonos();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 6:
                    ResultV2 resultV2 = new ResultV2();
                    resultV2.Percentage = 33;
                    Console.WriteLine(resultV2.ComputerHonors());
                    ResultV2 resultV2a = new ResultV2();
                    resultV2a.Percentage = 58;
                    Console.WriteLine(resultV2a.ComputerHonors());
                    ResultV2 resultV2b = new ResultV2();
                    resultV2b.Percentage = 65;
                    Console.WriteLine(resultV2a.ComputerHonors());
                    ResultV2 resultV2c = new ResultV2();
                    resultV2c.Percentage = 78;
                    Console.WriteLine(resultV2c.ComputerHonors());
                    ResultV2 resultV2d = new ResultV2();
                    resultV2d.Percentage = 99;
                    Console.WriteLine(resultV2d.ComputerHonors());
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 7:
                    NumberCombination pair = new NumberCombination();

                    Console.WriteLine("Geef getal 1 in:");
                    int number = int.Parse(Console.ReadLine());
                    Console.WriteLine("Geef gatal 2 in:");
                    int number1 = int.Parse(Console.ReadLine());

                    pair.Num1 = number;
                    pair.Num2 = number1;

                    Console.WriteLine($"Paar: {pair.Num1}, {pair.Num2}");
                    Console.WriteLine($"Sum: {pair.Num1} + {pair.Num2} == {pair.Sum()}");
                    Console.WriteLine($"Verschil: {pair.Num1} - {pair.Num2} == {pair.Difference()}");
                    Console.WriteLine($"product: {pair.Num1} * {pair.Num2} == {pair.Product()}");
                    Console.WriteLine($"Quotient: {pair.Num1} / {pair.Num2} == {pair.Quotient()}");
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 8:
                    FigurenProgram.Main();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 9:
                    Student studentKlasse = new Student();
                    studentKlasse.ClassGroupPro = Student.ClassGroup.EA1;
                    studentKlasse.Age = 22;
                    studentKlasse.Name = "Tom Tomas";
                    studentKlasse.MarkCommunication = 16;
                    studentKlasse.MarkProgrammingPrinciples = 15;
                    studentKlasse.MarkWebTech = 18;
                    studentKlasse.ShowOverview();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 10:
                    ResultV3 resultV3 = new ResultV3();
                    resultV3.Percentage = 33;
                    Console.WriteLine($"{resultV3.Honors}");
                    ResultV3 resultV3a = new ResultV3();
                    resultV3a.Percentage = 63;
                    Console.WriteLine($"{resultV3a.Honors}");
                    ResultV3 resultV3b = new ResultV3();
                    resultV3b.Percentage = 72;
                    Console.WriteLine($"{resultV3b.Honors}");
                    ResultV3 resultV3c = new ResultV3();
                    resultV3c.Percentage = 82;
                    Console.WriteLine($"{resultV3c.Honors}");
                    ResultV3 resultV3d = new ResultV3();
                    resultV3d.Percentage = 88;
                    Console.WriteLine($"{resultV3d.Honors}");
                    Console.ReadKey();
                    Console.Clear();
                    break;

                default:
                    Console.WriteLine("Ongeldige Keuze!");
                    //Console.ReadKey();
                    Console.Clear();
                    break;


            }
        }
    }
}
