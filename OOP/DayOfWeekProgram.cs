﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace OOP
{
    /*
    * Summay
     * key point 1: aanmaken van DateTime object.
     * key point 2: formatteren van DateTime object.
     * key point 3: using System.Globalization; CultureInfo.
     * key point 4: TimeSpan.(Ticks2000)
     * key point 5: { .Ticks}, "ticks" = 1/10000 milliseconden(.NET)
     * key point 6: IsLeapYear.(LeapYear)
     * key point 7: .TotalMilliseconds     
	 *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H8-dag-van-de-week.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Klassen en Objecten.Object Oriented Programming.
     * Graduaatsopleiding programmeren.
     * AP Hogeschool.

    */
    class DayOfWeekProgram
    {
        public static void DayOfWeek()
        {
            Console.WriteLine("welke dag?");
            int day = int.Parse(Console.ReadLine());
            Console.WriteLine("welke maand?");
            int month = int.Parse(Console.ReadLine());
            Console.WriteLine("welke jaar?");
            int year = int.Parse(Console.ReadLine());
            DateTime dateTime = new DateTime(year, month, day);
            CultureInfo cultureInfoBE = new CultureInfo("nl_BE");
            string info = $"{dateTime.ToString("dd MMMM yyyy", cultureInfoBE)}";
            info += $" is een {dateTime.ToString("dddd", cultureInfoBE)}";
            Console.WriteLine($"\r\n{info}");


        }

        public static void Ticks2000()
        {
            DateTime year2000 = new DateTime(2000, 1, 1);
            DateTime now = DateTime.Now;
            TimeSpan difference = now - year2000;
            Console.WriteLine($"Sindes 1 januari 2000 zijn er {difference.Ticks} ticks voorbijgegaan.");
        }

        public static void LeapYear()
        {
            int counter = 0;
            for (int i = 1800; i <= 2020; i++)
            {
                if (DateTime.IsLeapYear(i))
                {
                    counter++;
                }
            }
            Console.WriteLine($"Er zijn {counter} schrikkeljaren tussen 1800 en 2020.");
        }

        public static void ArrayTimer()
        {
            DateTime start = DateTime.Now;
            int[] array = new int[1000000];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = i + 1;
            }
            DateTime end = DateTime.Now;
            TimeSpan diff = end - start;
            Console.WriteLine($"Het duurt {diff.TotalMilliseconds} milliseconden om " +
                $"een array van een miljoen elementen aan te maken en op te vullen met opeenvolgende waarden.");
        }
    }
}
