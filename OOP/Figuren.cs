﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: class en objecten
     * key point 2: geldige waarden 
     *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H8-Figuren.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.

    */
    class Figuren
    {
    }

    class Rectangle
    {
        public double Height { get; set; }
        public double Width { get; set; }
        public double Surface
        {
            get
            {
                if (Height == 0)
                {
                    Console.WriteLine($"Het is verboden een hoogte van {Height} in te stellen!");
                }
                if (Height <= 0)
                {
                    Console.WriteLine($"Het is verboden een hoogte van {Height} in te stellen!");
                }
                if (Width == 0)
                {
                    Console.WriteLine($"Het is verboden een hoogte van {Width} in te stellen!");
                }
                if (Width == 0)
                {
                    Console.WriteLine($"Het is verboden een hoogte van {Width} in te stellen!");
                }
                return Height * Width;
            }
        }

    }

    class Triangle
    {
        public double Height { get; set; }
        public double Base { get; set; }
        public double Surface
        {
            get
            {
                if(Height == 0)
                {
                    Console.WriteLine($"Het is verboden een hoogte van {Height} in te stellen!");
                }
                if (Height <= 0)
                {
                    Console.WriteLine($"Het is verboden een hoogte van {Height} in te stellen!");
                }
                if (Base == 0)
                {
                    Console.WriteLine($"Het is verboden een hoogte van {Base} in te stellen!");
                }
                if (Base == 0)
                {
                    Console.WriteLine($"Het is verboden een hoogte van {Base} in te stellen!");
                }
                return Height * Base / 2;
            }
        }
    }

    class FigurenProgram
    {
        public static void Main()
        {
            Rectangle rectangle = new Rectangle();
            Triangle triangle = new Triangle();
            Console.ResetColor();
            Console.WriteLine("Wat wil je doen?");
            Console.WriteLine("1. Oppervlakte van een rechthoek");
            Console.WriteLine("2. Oppervlakte van een driehoek");
            Console.Write("\r\r\n\nKeuzen: ");
            int choice = int.Parse(Console.ReadLine());
            Console.Write("\r\n");
            switch (choice)
            {
                case 1:
                    Console.WriteLine("\r\nGeef hoogte van een rechthoek in:");
                    double height = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("\r\nGeef breedte van een rechthoek in:");
                    double width = Convert.ToDouble(Console.ReadLine());
                    rectangle.Height = height;
                    rectangle.Width = width;
                    Console.WriteLine($"\r\nEen rechthoek met een breedte van {rectangle.Width}m en " +
                        $"een hoogte van {rectangle.Height}m heeft een oppervlakte van {rectangle.Surface}m².");
                    break;
                case 2:
                    Console.WriteLine("\r\nGeef hoogte van een driehoek in:");
                    double heightTri = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("\r\nGeef breedte van een rechthoek in:");
                    double baseTri = Convert.ToDouble(Console.ReadLine());
                    triangle.Height = heightTri;
                    triangle.Base = baseTri; 
                    Console.WriteLine($"\r\nEen driehoek met een breedte van {triangle.Base}m en " +
                        $"een hoogte van {triangle.Height}m heeft een oppervlakte van {triangle.Surface}m².");
                    break;

                default:
                    Console.WriteLine("Ongeldige Keuze!");
                    Console.ReadKey();
                    Console.Clear();
                    break;
            }


        }
        
    }

}
