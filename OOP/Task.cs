﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Task
    {
        public string Description { get; set; }

        public Task() { }
        public Task(string description)
        {
            this.Description = description;
        }
        
    }

    class RecurringTask : Task
    {
        public string Beschrijving { get; set; }
        public byte Days { get; set; }

        public RecurringTask(string beschrijving, byte days, string description) : base(description)
        {
            this.Beschrijving = beschrijving;
            this.Days = days;
        }
    }


}
