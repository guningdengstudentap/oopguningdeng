﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Geometry
{
    /* 
	 * Summay
     * key point 1: add Class.
     * key point 2: bouw object by proprties.
     * key point 3: full property -- ConsoleColor datatype.
     * key point 4: full property -- char datatype.
     * key point 5: full properties met datatypes.
     * key point 6: creëer methods.
     * key point 7: creëer object by 'new'.
     * key point 8: geef value aan gecreëerde objects.
     * key point 9: gecreëerde objects ropen methodes op.
     * *************************************
	 * Guning Deng(student)
	 * Opdracht: (Klassikale!) smaakmaker OOP. H8-Vormen.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.
        
    */
    class ShapesBuilder
    {
        private ConsoleColor color;
        private char symbol;

        public ConsoleColor Color
        {
            get { return color; }
            set
            {
                color = value;
                Console.ForegroundColor = color;
            }
        }

        public char Symbol
        {
            get { return symbol; }
            set
            {
                if(symbol == null)
                {
                    symbol = '_';
                } else
                {
                    symbol = value;
                }
            }
        }

        public string Line(int length)
        {
            return new string(symbol, length);
        }

        public string Line(string length)
        {
            return new string(symbol, Convert.ToInt32(length));
        }

        public string Line(int length, char alternateSymbol)
        {
            return new string(alternateSymbol, length);
        }

        public string Rectangle(int height, int width)
        {
            return Rectangle(height, width, symbol);
        }

        public string Rectangle(int height, int width, char alternateSymbol)
        {
            string output = "";
            for(int i = 0; i < height; i++)
            {
                output += Line(width, alternateSymbol);
                if(i < height - 1)
                {
                    output += "\n";
                }
            }
            return output;
        }

        public string Triangle(int height)
        {
            return Triangle(height, symbol);
        }

        public string Triangle(int height, char alternateSymbol)
        {
            string output = "";
            for(int i = 0; i < height; i++)
            {
                output += Line(i + 1, alternateSymbol);
                if(i < height - 1)
                {
                    output += "\n";
                }
            }
            return output;
        }

        public static void ShowShapes()
        {
            ShapesBuilder shapesBuilder = new ShapesBuilder();
            ShapesBuilder shapesBuilder1 = new ShapesBuilder();

            shapesBuilder.Color = ConsoleColor.Yellow;
            shapesBuilder.Symbol = '*';
            string line = shapesBuilder.Line(10);
            Console.WriteLine($"{line}");

            shapesBuilder.Color = ConsoleColor.Red;
            string line2 = shapesBuilder.Line(15, '#');
            Console.WriteLine($"{line2}");

            shapesBuilder.Color = ConsoleColor.Green;
            string rectangle = shapesBuilder.Rectangle(5, 8);
            Console.WriteLine($"{rectangle}");
            shapesBuilder.Color = ConsoleColor.Blue;
            string rectangle1 = shapesBuilder.Rectangle(8, 5, '$');
            Console.WriteLine($"{rectangle1}");

            shapesBuilder.Color = ConsoleColor.Red;
            string traingle = shapesBuilder.Triangle(8);
            Console.WriteLine($"{traingle}");
            shapesBuilder.Color = ConsoleColor.Yellow;
            string traingle1 = shapesBuilder.Triangle(6, 'é');
            Console.WriteLine($"{traingle1}");
        }
                      
    }
}
