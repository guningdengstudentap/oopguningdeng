﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: .TotalMilliseconds
     *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H8-simpele-timing.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.

    */
    class ArrayTimerProgram
    {
        public static void Main()
        {
            DateTime start = DateTime.Now;
            int[] array = new int[1000000];
            for(int i = 0; i < array.Length; i++)
            {
                array[i] = i + 1;
            }
            DateTime end = DateTime.Now;
            TimeSpan diff = end - start;
            Console.WriteLine($"Het duurt {diff.TotalMilliseconds} milliseconden om " +
                $"een array van een miljoen elementen aan te maken en op te vullen met opeenvolgende waarden.");
        }
    }
}
