﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: IsLeapYear.
     *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H8-schrikkelteller.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.

    */
    class LeapYearProgram
    {
        public static void Main()
        {
            int counter = 0;
            for (int i = 1800; i <= 2020; i++)
            {
                if (DateTime.IsLeapYear(i))
                {
                    counter++;
                }
            }
            Console.WriteLine($"Er zijn {counter} schrikkeljaren tussen 1800 en 2020.");
        }
    }
}
