﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: class en objecten
     * key point 2: full properties 
     *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H8-Getallencombinatie.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.

    */
    class NumberCombination
    {
        private int num1;

        public int Num1
        {
            get { return num1; }
            set { num1 = value; }
        }
        private int num2;

        public int Num2
        {
            get { return num2; }
            set { num2 = value; }
        }


        public double Sum()
        {
            return num1 + num2;
        }
        public double Difference()
        {
            return num1 - num2;
        }
        public double Product()
        {
            return num1 * num2;
        }
        public double Quotient()
        {
            if(num2 == 0)
            {
                Console.WriteLine("Error");
            }
            return num1 / num2;
        }

    }

}
