﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: foreach genest
     * key point 2: list<> list.RemoveAt() list.Count 
     *************************************
	 * Guning Deng(student)
     * Opdracht: oefening h11 speelkaarten.
	 * Section: H11 Arrays en klassen.Object Oriented Programming. 
	 * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
     * Graduaatsopleiding programmeren.
     * AP Hogeschool.

    */

    class PlayingCard
    {
        private byte points;

        public byte Points
        {
            get { return points; }
            set
            {
                if (value >= 1 && value <= 13)
                {
                    points = value;
                }
            }
        }

        public Suites SuiteProp { get; set; }

        public PlayingCard(byte inPoints, Suites suite)
        {
            this.Points = inPoints;
            this.SuiteProp = suite;
        }

        public static List<PlayingCard> GenerateDeck()
        {
            var cards = new List<PlayingCard>();
            foreach (Suites suite in new Suites[] { Suites.Club, Suites.Diamond, Suites.Heart, Suites.Spade })
            {
                Console.WriteLine($"{suite}");
                for (byte i = 1; i <= 13; i++)
                {
                    PlayingCard card = new PlayingCard(i, suite);
                    cards.Add(card);
                }
            }
            return cards;
        }

        private static Random random = new Random();
        public static void ShowShuffledCeck(List<PlayingCard> cards)
        {
            while (cards.Count > 0)
            {
                int index = random.Next(0, cards.Count);
                PlayingCard card = cards[index];
                cards.RemoveAt(index);
                Console.WriteLine($"de kaart heeft type {card.SuiteProp} en getalwaarde {card.Points}");
            }

        }

    }
}
