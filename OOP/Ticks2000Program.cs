﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: aanmaken van DateTime object.
     * key point 2: TimeSpan.
     * key point 3: { .Ticks}, "ticks" = 1/10000 milliseconden(.NET)
	 *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H8-ticks-sinds-2000.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.

    */
    class Ticks2000Program
    {
        public static void Main()
        {
            DateTime year2000 = new DateTime(2000, 1, 1);
            DateTime now = DateTime.Now;
            TimeSpan difference = now - year2000;
            Console.WriteLine($"Sindes 1 januari 2000 zijn er {difference.Ticks} ticks voorbijgegaan.");
        }
    }
}
