﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /* 
	 * Summay
     * key point 1: public static Random random = new Random();
     
	 *************************************
	 * Guning Deng(student)
	 * Opdracht: (Klassikale!) smaakmaker OOP. H8-Honden
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.
             
    */
    class BarkingDog
    {
        public static Random random = new Random();
        int breedNum1 = random.Next(0, 3);
        int breedNum2 = random.Next(0, 3);

        public string Name { get; set; }

        public string Breed
        {
            get
            {
                if (breedNum1 == 0)
                {
                    return "German Shepherd";
                }
                else if (breedNum1 == 1)
                {
                    return "Wolfspitz";
                }
                else
                {
                    return "Chihuahua";
                }
            }
        }

        public string Bark()
        {
            if (Breed == "German Shepherd")
            {
                return $"RUFF! is {Breed}";
            }
            else if (Breed == "Wolfspitz")
            {
                return $"AwawaWAF! is {Breed}";
            }
            else if (Breed == "Chihuahua")
            {
                return $"ARF ARF ARF! is {Breed}";
            }
            else
            {
                return "Euhhh... Miauw?";
            }
        }

        public static void Barking()
        {
            string[] dogName = new string[] { "Swiber", "Misty", "Tom", "Fatcat", "Kim", "Snopy" };
            Random random = new Random();
            BarkingDog dog1 = new BarkingDog();
            dog1.Name = dogName[random.Next(0, 5)];
            BarkingDog dog2 = new BarkingDog();
            dog2.Name = dogName[random.Next(0, 5)];
            Console.WriteLine($"\n{dog1.Name} barks: {dog1.Bark()}\n\r");
            Console.WriteLine($"{dog2.Name} barks: {dog2.Bark()}\n\r");
        }

    }  

}
