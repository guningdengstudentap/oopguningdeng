﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: foreach
     * key point 2: double item, {item:f2} of item.ToString("f2"). Twee decimalen.
     *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H11-prijzen met foreach (h11-prijzen).
	 * Section: H11-Arrays en klassen.Object Oriented Programming. 
	 * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
     * Graduaatsopleiding programmeren.
     * AP Hogeschool.

    */
    class ArrayEnKlassen
    {
        public static void AskForPrices()
        {
            double[] price = new double[20];
            Console.WriteLine("Gelieve 20 prijzen in te geven");
            Random random = new Random();
            int priceRandom;
            double sum = 0;
            for (int i = 0; i < price.Length; i++)
            {
                priceRandom = random.Next(1, 110);
                price[i] = Convert.ToDouble(priceRandom); // generate 20 numbers
                Console.WriteLine($"Price {i + 1}: {price[i]}");
            }

            foreach (double value in price)
            {
                if (value >= 5)
                {
                    Console.WriteLine($"\r\nPrijs hoger dan 5:");
                    Console.WriteLine($"{value:f2}");
                }
                sum += value;

            }

            Console.WriteLine($"\r\nHet gemiddelde bedrag is: {sum / price.Length:f2}");
            Console.WriteLine($"\r\nDe sum is: {sum:f2}");

        }

        public static void ExecuteStudentMenu()
        {
            var studentList = new List<Student>();
            byte choiceNum;
            do
            {
                Console.ResetColor();
                Console.WriteLine("\nWelke optie wil je uitvoeren?");
                Console.WriteLine("1, gegevens van de studenten tonen");
                Console.WriteLine("2, een nieuwe student toevoegen");
                Console.WriteLine("3, gegevens van een bepaalde student aanpassen");
                Console.WriteLine("4, gegevens van een bepaalde student verwijderen");
                Console.WriteLine("5, stoppen");
                Console.Write("\n\r");
                choiceNum = byte.Parse(Console.ReadLine());
                switch (choiceNum)
                {
                    case 1:
                        foreach(Student student in studentList)
                        {
                            student.ShowOverview();
                        }
                        break;
                    case 2:
                        Student studentX = new Student();
                        studentX.Name = "XXX XXX";
                        studentX.Age = 21;
                        studentX.ClassGroupPro = Student.ClassGroup.EA1;
                        studentX.MarkCommunication = 15;
                        studentX.MarkProgrammingPrinciples = 17;
                        studentX.MarkWebTech = 11;
                        studentList.Add(studentX);
                        break;
                    case 3:
                        Console.WriteLine("Wat is de indexpositie van de student die je wil aanpassen?");
                        int index = Convert.ToInt32(Console.ReadLine());
                        Student modifiedStudent = studentList[index];
                        Console.WriteLine("\nWat wil je aanpassen?");
                        Console.WriteLine("1. Name.");
                        Console.WriteLine("2. Leeftijd.");
                        Console.WriteLine("3. Klasgroep.");
                        Console.WriteLine("4. Cijfer communicatie.");
                        Console.WriteLine("5. Cijfer programmeren.");
                        Console.WriteLine("6. Cijfer webtechnologie.");
                        Console.Write("\r\n>: ");
                        byte modified = Convert.ToByte(Console.ReadLine());
                        switch (modified)
                        {
                            case 1:
                                modifiedStudent.Name = Console.ReadLine();
                                break;
                            case 2:
                                modifiedStudent.Age = Convert.ToByte(Console.ReadLine());
                                break;
                            case 3:
                                modifiedStudent.ClassGroupPro = (Student.ClassGroup)Convert.ToInt32(Console.ReadLine());
                                break;
                            case 4:
                                modifiedStudent.MarkCommunication = Convert.ToByte(Console.ReadLine());
                                break;
                            case 5:
                                modifiedStudent.MarkProgrammingPrinciples = Convert.ToByte(Console.ReadLine());
                                break;
                            case 6:
                                modifiedStudent.MarkWebTech = Convert.ToByte(Console.ReadLine());
                                break;

                            default:
                                break;
                        }
                        break;
                    case 4:
                        Console.WriteLine("Wat is de indexpositie van de student die je wil verwijderen?");
                        int removeIndex = Convert.ToInt32(Console.ReadLine());
                        Student removeStudent = studentList[removeIndex];
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 5:
                        Console.WriteLine("Exit");
                        // Console.ReadKey();
                        Console.Clear();
                        return;

                    default:
                        Console.WriteLine("Ongeldige keuze!");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }
            } while (choiceNum != 5);

        }


        public static void StartSubmenuArrayEnKlssen()
        {
            Console.ResetColor();
            Console.WriteLine("\nWelke oefening wil je uitvoeren?");
            Console.WriteLine("1. Prijzen met foreach");
            Console.WriteLine("2. Playing Card");
            Console.WriteLine("3. Studenten");
            Console.Write("**************************\n\n\r\r");
            int choice = Convert.ToInt32(Console.ReadLine());
            Console.Write("\n\r");
            switch (choice)
            {
                case 1:
                    AskForPrices();
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 2:
                    PlayingCard.ShowShuffledCeck(PlayingCard.GenerateDeck());
                    Console.ReadKey();
                    Console.Clear();
                    break;
                case 3:
                    ExecuteStudentMenu();
                    Console.ReadKey();
                    Console.Clear();
                    break;

                default:
                    Console.WriteLine("Ongeldige keuze!");
                    Console.ReadKey();
                    Console.Clear();
                    break;
            }

        }
    }
}
