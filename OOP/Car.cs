﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
	/* 
	 * Summay
     * key point 1: add Class Car
     * key point 2: bouw een simulatie van rijdende auto's
     * key point 3: voorzie een kilometerteller, een huidige snelheid, een methode om de gaspedaal te simuleren en om de rem te simuleren.
     * key point 4: method Gas() om snelheid 10 km/u te verhogen, method Brake() om snelheid 10 km/u te verlagen.
     * key point 5: snelheid mag niet hooger dan 120 km/u. gemiddelde snelheid is 60 km/u. 
     * key point 6: maak twee cars. de eerst car vijf keer laat versnellen zn drie keer remmen. de tweede laat 10 keer versnellen en een keer remmen.
	 * key point 7: door ':f2' om twee decimalen nemen.
	 *************************************
	 * Guning Deng(student)
	 * Opdracht: (Klassikale!) smaakmaker OOP. H8-Auto.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.
             
    */
	class Car
    {
		private double speed;

		public double Speed
		{
			get { return speed; }
			set
			{
				if(value >= 0 && value <= 120)
				{
					speed = value;
				} else if(value < 0)
				{
					speed = 0;
				}
				else
				{
					speed = 120;
				}
			}
		}

		private double odometer;

		public double Odometer
		{
			get { return odometer; }
			set { odometer = value; }
		}
		public void Gas()
		{
			double oldSpeed = speed;
			Speed += 10;
			Odometer += ((odometer + speed) / 2) / 60;
		}

		public void Brake()
		{
			double oldSpeed = speed;
			Speed -= 10;
			Odometer += ((odometer + speed) / 2) / 60;
		}

		public static void DrivingCars()
		{
			Car car = new Car();
			Car car1 = new Car();
			for (int i = 0; i < 4; i++)
			{
				car.Gas();
			}
			for (int i = 0; i < 2; i++)
			{
				car.Brake();
			}

			for (int i = 0; i < 9; i++)
			{
				car1.Gas();
			}
			car1.Brake();

			Console.WriteLine($"\nAuto 1: {car.Speed:f2}km/u, afgelegde weg {car.Odometer:f2}km.\n\r");
			Console.WriteLine($"Auto 2: {car1.Speed:f2}km/u, afgelegde weg {car1.Odometer:f2}km.\n\r");

		}
	}

}
