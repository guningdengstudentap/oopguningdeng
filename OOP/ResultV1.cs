﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: properties en methodes.
     * key point 2: maak 'no return' methode door 'public void + 'methode name'(){}'
     *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H8-RapportModule-V1.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.

    */
    class ResultV1
    {
        public byte Percentage { get; set; }

        public void PrintHonos()
        {
            if(Percentage < 50)
            {
                Console.WriteLine("Niet geslaagd.");
            } else if(Percentage <= 68)
            {
                Console.WriteLine("Voldoende.");
            } else if(Percentage <= 75)
            {
                Console.WriteLine("Onderscheiding.");

            } else if(Percentage <= 85)
            {
                Console.WriteLine("Grote onderscheiding.");
            }
            else
            {
                Console.WriteLine("Grootsts onderscheiding.");
            }
        }
    }

    class ResultV1Program
    {
        public static void Main()
        {
            ResultV1 resultV1 = new ResultV1();
            resultV1.Percentage = 48;
            resultV1.PrintHonos();
            ResultV1 resultV1a = new ResultV1();
            resultV1a.Percentage = 55;
            resultV1a.PrintHonos();
            ResultV1 resultV1b = new ResultV1();
            resultV1b.Percentage = 74;
            resultV1b.PrintHonos();
            ResultV1 resultV1c = new ResultV1();
            resultV1c.Percentage = 92;
            resultV1c.PrintHonos();
            ResultV1 resultV1d = new ResultV1();
            resultV1d.Percentage = 81;
            resultV1d.PrintHonos();


        }
    }
}
