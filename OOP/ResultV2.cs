﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: properties en methodes.
     * key point 2: enum en door een method 'return' de waarden van enum 
     *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H8-RapportModule-V2.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.

    */
    class ResultV2
    {
        public byte Percentage { get; set; }

        public Honors ComputerHonors()
        {
            if(Percentage < 50)
            {
                return Honors.NietGeslaagd;
            } else if(Percentage <= 68)
            {
                return Honors.Voldoende;
            } else if(Percentage <= 75)
            {
                return Honors.Onderscheiding;
            } else if(Percentage <= 85)
            {
                return Honors.GroteOnderscheiding;
            }
            else
            {
                return Honors.GrootsteOnderscheiding;
            }
        }
    }

    class ResultV2Program
    {
        public static void Main()
        {
            ResultV2 resultV2 = new ResultV2();
            resultV2.Percentage = 33;
            Console.WriteLine(resultV2.ComputerHonors());
            ResultV2 resultV2a = new ResultV2();
            resultV2a.Percentage = 58;
            Console.WriteLine(resultV2a.ComputerHonors());
            ResultV2 resultV2b = new ResultV2();
            resultV2b.Percentage = 65;
            Console.WriteLine(resultV2a.ComputerHonors());
            ResultV2 resultV2c = new ResultV2();
            resultV2c.Percentage = 78;
            Console.WriteLine(resultV2c.ComputerHonors());
            ResultV2 resultV2d = new ResultV2();
            resultV2d.Percentage = 99;
            Console.WriteLine(resultV2d.ComputerHonors());
        }
    }

}
