﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /* 
	 * Summay
     * key point 1: refactoring.
     * key point 2: door refactoring zo een meer objectgericht en beter onderhoudbaar programma te bekomen.
     * key point 3: object array.
	 *************************************
	 * Guning Deng(student)
	 * Opdracht: (Klassikale!) smaakmaker OOP. H8-Patien.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.
             
    */
    class Patient
    {
        // public enum PatientGender { man, vrouw }
        private string patientGender;

        public string PatientGenderPro
        {
            get { return patientGender; }
            set { patientGender = value; }
        }

        private string patientNames;

        public string PatientNames
        {
            get { return patientNames; }
            set { patientNames = value; }
        }

        private string patientLifestyles;

        public string PatientLifestyles
        {
            get { return patientLifestyles; }
            set { patientLifestyles = value; }
        }

        private int patientDays;

        public int PatientDays
        {
            get { return patientDays; }
            set { patientDays = value; }
        }

        private int patientMonths;

        public int PatientMonths
        {
            get { return patientMonths; }
            set { patientMonths = value; }
        }

        private int patientYears;

        public int PatientYears
        {
            get { return patientYears; }
            set { patientYears = value; }
        }

        public int Hour { get; set; }
        public Patient() { }

        public static void Showpatient()
        {
            Patient patient = new Patient();
            Console.WriteLine("Hoe veel patiëten zijn er?");
            int numberOfPatients = int.Parse(Console.ReadLine());
            string[] patientNames = new string[numberOfPatients];
            string[] patientGenders = new string[numberOfPatients];
            string[] patientLifestyles = new string[numberOfPatients];
            int[] patientDays = new int[numberOfPatients];
            int[] patientMonths = new int[numberOfPatients];
            int[] patientYears = new int[numberOfPatients];

            for (int i = 0; i < numberOfPatients; i++)
            {
                Console.WriteLine($"geef de naam van patiënt {i + 1}");
                patientNames[i] = Console.ReadLine();
                patient.PatientNames = patientNames[i];

                Console.WriteLine($"geef de lefestyle van patiënt {i + 1}");
                patientLifestyles[i] = Console.ReadLine();
                patient.PatientLifestyles = patientLifestyles[i];

                Console.WriteLine($"geef de gender van patiënt {i + 1}: V of M");
                patientGenders[i] = Console.ReadLine();
                patient.PatientGenderPro = patientGenders[i];

                Console.WriteLine($"welke dag is patiënt {i + 1} geboren?");
                patientDays[i] = int.Parse(Console.ReadLine());
                patient.PatientDays = patientDays[i];

                Console.WriteLine($"welke maand is patiënt {i + 1} geboren?");
                patientMonths[i] = int.Parse(Console.ReadLine());
                patient.PatientMonths = patientMonths[i];

                Console.WriteLine($"welke jaar is patiënt {i + 1} geboren?");
                patientYears[i] = int.Parse(Console.ReadLine());
                patient.PatientYears = patientYears[i];
            }

            for (int i = 0; i < numberOfPatients; i++)
            {
                string info = $"{patient.PatientNames = patientNames[i]} ({patient.PatientLifestyles = patientLifestyles[i]}, {patient.PatientGenderPro = patientGenders[i]})";
                info += $", geboren {patient.PatientDays = patientDays[i]}-{patient.PatientMonths = patientMonths[i]}-{patient.PatientYears = patientYears[i]}";
                Console.WriteLine("\r\r\n\n**************************************\r\n");
                Console.WriteLine($"{info}");
            }
        }

        public virtual void ShowCost()
        {
            int hours = Hour;
            int getCost;
            if (hours < 1) { hours = 1; }
            getCost = 50 + hours * 20;
            Console.WriteLine($"{PatientNames}, een gewone patiënt die {Hour} uur in het ziekenhuis gelegen heeft, betaalt \u20AC {getCost}");
        }

    }

    class InsuredPatient : Patient
    {

        public InsuredPatient(string name, int hour) : base() { }

        public override void ShowCost()
        {
            // base:ShowCost();
            int hours = Hour;
            int getCost;
            if (hours < 1) { hours = 1; }
            getCost = 50 + hours * 20;
            Console.WriteLine($"{PatientNames}, een verzekerde patiënt die {Hour} uur in het ziekenhuis gelegen heeft, betaalt \u20AC {getCost * 0.9}");
        }
    }




}
