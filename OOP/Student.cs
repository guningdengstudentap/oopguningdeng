﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    /*
    * Summay
     * key point 1: class en objecten
     * key point 2: geldige waarden
     * key point 3: byte is altijd minstens 0
     * key point 4: Convert.ToDouble('byteValue')
     * key point 5: {'doubleValue':f1} = 0,0
     * key point 6: geeft waarde door berekening binnen read-only property
     *************************************
	 * Guning Deng(student)
     * Opdracht: oefening H8-StudentKlasse.
	 * Section: Klassen en Objecten
     * Object Oriented Programming.
	 * Handboek:https://apwt.gitbook.io/cursus-pro-oo/
	 * Graduaatsopleiding programmeren.
	 * AP Hogeschool.

    */
    class Student
    {
        public enum ClassGroup { EA1, EA2, EA3, EB1, EB2, EB3 }

        public ClassGroup ClassGroupPro { get; set; }
        public string Name { get; set; }
        
        private byte age;
        public byte Age
        {
            get { return age; }
            set
            {
                if(value > 120)
                {
                    Console.WriteLine("Error");
                }
                age = value;
            }
        }

        private byte markCommunication;

        public byte MarkCommunication
        {
            get { return markCommunication; }
            set
            {
                if(value > 20)
                {
                    Console.WriteLine("Error");
                }
                markCommunication = value;
            }
        }

        private byte markProgrammingPrinciples;

        public byte MarkProgrammingPrinciples
        {
            get { return markProgrammingPrinciples; }
            set 
            {
                if (value > 20)
                {
                    Console.WriteLine("Error");
                }
                markProgrammingPrinciples = value; 
            }
        }

        private byte markWebTech;

        public byte MarkWebTech
        {
            get { return markWebTech; }
            set 
            {
                if (value > 20)
                {
                    Console.WriteLine("Error");
                }
                markWebTech = value; 
            }
        }

        public double OverallMark 
        { 
            get { return (Convert.ToDouble(markCommunication) + Convert.ToDouble(markProgrammingPrinciples) +
                    Convert.ToDouble(markWebTech)) / 3; } 
        }

        public virtual void ShowOverview()
        {
            Console.WriteLine("\r\nResultaten\r\n");
            Console.WriteLine("**********************************");
            Console.WriteLine($"{Name}, {Age}");
            Console.WriteLine($"Klas: {ClassGroupPro}");
            Console.WriteLine("\r\r\n\nCijferRapport:");
            Console.WriteLine("************************");
            Console.WriteLine($"Communicatie: {MarkCommunication}");
            Console.WriteLine($"Programming Principles: {MarkProgrammingPrinciples}");
            Console.WriteLine($"Web Technology: {MarkWebTech}");
            Console.WriteLine($"Gemiddelde: {OverallMark:f1}");

        }

        

    }

    class WorkingStudent : Student
    {
        private byte worHours;

        public byte WorkHours
        {
            get { return worHours; }
            set
            {
                if (value > 20)
                {
                    value = 10;
                }
                else
                {
                    worHours = value;
                }
            }
        }

        public bool HasWorkToday
        {
            get
            {
                bool[] arrHasWork = { true, false };
                Random random = new Random();
                return arrHasWork[random.Next(2)];
            }
        }

        public override void ShowOverview()
        {
            base.ShowOverview();
            Console.WriteLine($"Statuut: werkstudent");
            Console.WriteLine($"Antal werkuren per week: {WorkHours}");
            if (HasWorkToday == true)
            {
                Console.WriteLine($"Is vandaag werkdag: Ja");
            }
            else
            {
                Console.WriteLine($"Is vandaag werkdag: Nee");
            }

        }
    }


}
